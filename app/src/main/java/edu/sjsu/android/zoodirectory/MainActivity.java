package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    List<Animal> animalList = new ArrayList<Animal>();
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initList();
        ListView lv = (ListView)findViewById(R.id.listView);

        lv.setAdapter(new CustomAdapter(this, R.layout.listview_layout, animalList));


        // Going to the next activity
        //Intent myActivity = new Intent(action,data);
        //startActivity(myActivity);



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 4){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);

                    // set title
                    alertDialogBuilder.setTitle("WARNING");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("The animal that you have clicked, 'J**********', is extrememly dangerous. Do you still wish to proceed?")
                            .setCancelable(false)
                            .setPositiveButton("YES",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    Intent intent = new Intent(MainActivity.this, AnimalDetailActivity.class);
                                    intent.putExtra("title", animalList.get(4).getName());
                                    intent.putExtra("file", animalList.get(4).getFilename());
                                    intent.putExtra("description", animalList.get(4).getDescription());
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("No..",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else{
                    //Toast.makeText(MainActivity.this, "hello" + position + " position ", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, AnimalDetailActivity.class);
                    intent.putExtra("title", animalList.get(position).getName());
                    intent.putExtra("file", animalList.get(position).getFilename());
                    intent.putExtra("description", animalList.get(position).getDescription());
                    startActivity(intent);
                }
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_info) {
            Intent intent = new Intent(MainActivity.this, ZooContact.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_uninstall) {
            Intent myActivity2= new Intent(Intent.ACTION_DELETE);
            startActivity(myActivity2);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initList(){
        animalList.add(new Animal("Pegasus", "pegasus.jpg", "A mythical winged divine horse that's the offspring of the Olympian god Poseidon."));
        animalList.add(new Animal("Phoenix", "phoenix.png", "A long-lived bird that cyclically regenerates or is otherwise born again."));
        animalList.add(new Animal("Dragon", "dragon.png", "A large, serpentine legendary creature that appears in the folklore of many cultures around the world"));
        animalList.add(new Animal("Hydra", "hydra.jpeg", "An immortal serpent-like monster with heads that grow twofold when cut."));
        animalList.add(new Animal("Jabberwocky", "jabber.jpg", "A a ferocious monster from Alice in Wonderland. BEWARE"));
    }

}
