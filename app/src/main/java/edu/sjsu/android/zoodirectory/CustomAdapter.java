package edu.sjsu.android.zoodirectory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import java.io.InputStream;
import java.io.IOException;

public class CustomAdapter extends ArrayAdapter<Animal>  {

    private final List<Animal> zoo;

    public CustomAdapter(Context context, int resource, List<Animal> zoo) {
        super(context, resource, zoo);
        this.zoo = zoo;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Animal animal = zoo.get(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.listview_layout, null);
        TextView textView = (TextView) row.findViewById(R.id.rowText);
        textView.setText(animal.getName());
        try {
            ImageView imageView = (ImageView) row.findViewById(R.id.rowImage);
            InputStream inputStream = getContext().getAssets().open(animal.getFilename());
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageView.setImageDrawable(drawable);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return row;

    }
}





