package edu.sjsu.android.zoodirectory;

import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;

public class ZooContact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zoo_contact_layout);
    }

    public void onClick(View view) { switch (view.getId()) {
        case R.id.button:
            String myPhoneNumberUri ="tel:666-6666";
            Intent myActivity2= new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
            startActivity(myActivity2);
            break;
        }
    }
}
