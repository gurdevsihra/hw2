package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

public class AnimalDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_detail_layout);

        AssetManager assetManager = getAssets();

        Bundle intent = this.getIntent().getExtras();
        String title = intent.getString("title");
        String file = intent.getString("file");
        String descr = intent.getString("description");

        TextView titleText = (TextView)findViewById(R.id.title);
        titleText.setText(title);

        try {
            ImageView imageView = (ImageView)findViewById(R.id.image);
            InputStream inputStream = assetManager.open(file);
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            imageView.setImageDrawable(drawable);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView descText = (TextView)findViewById(R.id.description);
        descText.setText(descr);

    }
}
