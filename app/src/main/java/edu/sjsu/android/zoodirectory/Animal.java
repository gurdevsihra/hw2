package edu.sjsu.android.zoodirectory;

public class Animal {
    String name;
    String pic;
    String description;

    public Animal(String name, String pic, String description){
        this.name = name;
        this.pic = pic;
        this.description = description;
    }

    public String getName(){
        return name;
    }

    public String getFilename(){
        return pic;
    }

    public String getDescription(){
        return description;
    }
}
